import './style.css';
import { el, setChildren } from "redom";
import valid from "card-validator";

const wrapper = el('div.wrapper'), 
    form = el('form.form', { action: '#' }),
    date = el('input.inp-date.inp', { type: 'text' }),
    num = el('input.inp-num.inp', { type: 'text' }),
    cardNumber = el('input.inp-card-number.inp', { type: "text" }),
    email = el('input.inp-email.inp', { type: 'text' }),
    errWrapper = el('div.error-wrapper'),
    btnPay = el('button.btn.disabled', 'Оплатить', { type: 'submit' }),
    cardTypeSpan = el('span.card-type-span'),
    errorArray = [];

setChildren(wrapper, cardTypeSpan, errWrapper, form)
setChildren(form, [cardNumber, date, num, email, btnPay])
setChildren(document.body, wrapper)

cardNumber.addEventListener('input', function(event) {
    errWrapper.innerHTML = ''

    if(valid.number(cardNumber.value).card) {
        checkTypeCard(valid.number(cardNumber.value).card.type)
    } else {
        if(cardTypeSpan.innerHTML !== '') {
            cardTypeSpan.innerHTML = ''
        }
    }

    event.target.value = event.target.value
    .replace(/(\d{4})(?!\s|$)/gm, `$1 `)
    .match(/(?:\d{4} ?){0,3}(?:\d{0,4})?/)

    if(btnPay.classList.contains('disabled')) {
        return
    } else btnPay.classList.add('disabled')
})

function checkTypeCard(type) {
    if(type === 'visa') {
        setChildren(cardTypeSpan, el('img.logo', { src: './img/logo-visa.png'}))
    }

    if(type === 'mastercard') {
        setChildren(cardTypeSpan, el('img.logo', { src: './img/logo-mastercard.png'}))
    }

    if(type === 'unionpay') {
        setChildren(cardTypeSpan, el('img.logo', { src: './img/logo-unionpay.png'}))
    }
}

cardNumber.addEventListener('blur', () => {
    if(valid.number(cardNumber.value).isValid === false) {
        if(errorArray.includes('cardNumber')) {
            checkError(errorArray)
            return
        } else errorArray.push('cardNumber') 
    } else {
        errorArray.splice(errorArray.indexOf('cardNumber'), 1)
    }

    checkError(errorArray)
})

date.addEventListener('input', function(event) {
    errWrapper.innerHTML = ''

    const inputLength = event.target.value.length;
    if(inputLength === 2){
      event.target.value += '/'
    }
    
    if(btnPay.classList.contains('disabled')) {
        return
    } else btnPay.classList.add('disabled')
})

date.addEventListener('blur', () => {
    const res = date.value.split('/')

    let mounthValidation = valid.expirationMonth(res[0]),
        yearValidation = valid.expirationYear(res[1]);

    if(mounthValidation.isValid === false || yearValidation.isValid === false) {
        if(errorArray.includes('date')) {
            checkError(errorArray)
            return
        } else errorArray.push('date') 
    } else {
        errorArray.splice(errorArray.indexOf('date'), 1)
    }

    checkError(errorArray)

})

num.addEventListener('input', function (event) {
    event.target.value = event.target.value.replace(/[^\d]/,'');

    if(btnPay.classList.contains('disabled')) {
        return
    } else btnPay.classList.add('disabled')
})

num.addEventListener('blur', () => {
    if(valid.cvv(num.value).isValid === false) {
        if(errorArray.includes('num')) {
            checkError(errorArray)
            return
        } else {
            errorArray.push('num')
        } 
    } else {
        errorArray.splice(errorArray.indexOf('num'), 1)
    }

    checkError(errorArray)
})

email.addEventListener('input', () => {
    if(btnPay.classList.contains('disabled')) {
        return
    } else btnPay.classList.add('disabled')
})

email.addEventListener('blur', () => {
    if(email.value.length < 2 || email.value.includes('@') === false) {
        if(errorArray.includes('email')) {
            checkError(errorArray)
            return
        } else {
            errorArray.push('email')
        } 
    } else {
        errorArray.splice(errorArray.indexOf('email'), 1)
    }

    checkError(errorArray)
})

function checkError(arr) {
    for(const element of arr) {
        if(element === 'cardNumber') {
            setChildren(errWrapper, el('span.error', 'Карты с таким номером не существует'))
            return
        }

        if(element === 'date') {
            setChildren(errWrapper, el('span.error', 'Дата введена неккоректно'))
            return
        }

        if(element === 'num') {
            setChildren(errWrapper, el('span.error', 'Номер должен содержать три цифры'))
            return
        }

        if(element === 'email') {
            setChildren(errWrapper, el('span.error', 'Email должен содерать не менее 2-х симолов и иметь символ "@"'))
            return
        }
    }

    if(cardNumber.value !== '' && num.value !== '' && email.value !== '' && date.value !== '') btnPay.classList.remove('disabled')
}